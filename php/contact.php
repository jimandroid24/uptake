<?php
session_start();

//Variables passed by the form
$fullname 	= filter_input(INPUT_POST, 'fullname', FILTER_SANITIZE_STRING);
$email		= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
$message 	= filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING);
$captch 	= filter_input(INPUT_POST, 'captcha_text', FILTER_SANITIZE_STRING);

if(!empty($email) && !empty($message)) {
		
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		echo 'E-mail is not valid';
	} else if(empty($captch)) {
		
		echo 'Please input captch';
		
	} else {
		if($_SESSION['security_captcha'] != $captch) {
			echo 'Please input correct captcha';
			exit;
		}
		// Variables for the 'mail' function
		$to = 'support@browsepres.com'; // Change this to your own e-mail
		$subject = 'A message from my website'; // Change the subject to whatever you like
		$from = 'This Message From: ' . $email;

		if(mail($to, $subject, $message, $from)) {
			echo 'Your message has been sent successfully';
		} else {
			echo 'Something went wrong';
		}
		
		$_SESSION['security_captcha'] = ''; // reset captcha
	}

} else {
	echo 'Please Fill All Required Fields';
}
