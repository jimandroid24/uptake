<?php
session_start();

/*
* File: CaptchaSecurityImages.php
* Author: Simon Jarvis
* Copyright: 2006 Simon Jarvis
* Date: 03/08/06
* Updated: 07/02/07
* Requirements: PHP 4/5 with GD and FreeType libraries
* Link: http://www.white-hat-web-design.co.uk/articles/php-captcha.php
* Monofont: http://www.coolfonts.info/font-8133-monofonto.php
* 
* This program is free software; you can redistribute it and/or 
* modify it under the terms of the GNU General Public License 
* as published by the Free Software Foundation; either version 2 
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNU General Public License for more details: 
* http://www.gnu.org/licenses/gpl.html
*
*/

class CaptchaSecurityImages {

	var $_font = '';
	var $_security = 'security_captcha';
	
	/****
	 * Constructor
	 */ 
	function __construct() {
		$this->_font = dirname(__FILE__) . '/MONOFONT.TTF';
	}
	
	/*************
	 * Set custom font
	 * 
	 * @param string $font: Path of font file
	 * @return void
	 * 
	 */
	function setFont($font='') {
		if(!empty($font) && is_file($font)) {
			$this->_font = $font;
		}
	}
	
	/**
	 * Set security name field
	 * 
	 * @param string $field: Name of field
	 * @return void 
	 */
	function setSecurityName($field = 'security_captcha') {
		$this->_security = $field;
	}
	
	/**
	 * Get security value
	 * 
	 * @return session security value
	 */
	function getSecurityValue() {
		if(empty($_SESSION[$this->security_name])) {
			return false;
		}
		return $_SESSION[$this->security_name];
	}
	
	/**************
	 * Get random character
	 * 
	 * @param int $characters: Max number of characters 
	 * @return string: Result of random string
	 * 
	 */
	function generateCode($characters) {
		/* list all possible characters, similar looking characters and vowels have been removed */
		$possible = '23456789bcdfghjkmnpqrstvwxyz';
		$code = '';
		$i = 0;
		while ($i < $characters) { 
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		return $code;
	}
	
	/**
	 * Generate image binary
	 * 
	 */
	function getImage($width='120',$height='40',$characters='6') {
		$code = $this->generateCode($characters);
		/* font size will be 50% of the image height */
		$font_size = $height * 0.50;
		$image = @imagecreate($width, $height) or die('Cannot initialize new GD image stream');
		/* set the colours */
		$background_color = imagecolorallocate($image, 255, 255, 255);
		$text_color = imagecolorallocate($image, 175, 153, 135);
		$noise_color = imagecolorallocate($image, 228, 221, 214);
		/* generate random dots in background */
		for( $i=0; $i<($width*$height)/3; $i++ ) {
			imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
		}
		/* generate random lines in background */
		for( $i=0; $i<($width*$height)/150; $i++ ) {
			imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
		}
		/* create textbox and add text */
		$textbox = imagettfbbox($font_size, 0, $this->_font, $code) or die('Error in imagettfbbox function');
		$x = ($width - $textbox[4])/2;
		$y = ($height - $textbox[5])/2;
		imagettftext($image, $font_size, 0, $x, $y, $text_color, $this->_font , $code) or die('Error in imagettftext function');
		
		/* output captcha image to browser */
		header('Content-Type: image/jpeg');
		imagejpeg($image);
		imagedestroy($image);
		
		$_SESSION[$this->_security] = $code;
	}

}

$width = isset($_GET['w']) ? $_GET['w'] : '120';
$height = isset($_GET['h']) ? $_GET['h'] : '40';
$characters = isset($_GET['c']) && $_GET['c'] > 1 ? $_GET['c'] : '6';

$captcha = new CaptchaSecurityImages();
$captcha->getImage($width,$height,$characters);

